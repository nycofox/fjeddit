<?php

/*
 * Admin routes.
 * Requires the user to have the admin-flag
 */

Route::group(['middleware' => 'admin', 'prefix' => 'admin'], function () {

    Route::get('/', 'Admin\DashboardController@index')->name('admin.dashboard');

    Route::get('/webcomics', 'Admin\WebcomicController@showDashboard')->name('admin.webcomics.dashboard');
    Route::get('/webcomics/list', 'Admin\WebcomicController@index')->name('admin.webcomics.index');
    Route::post('/webcomics', 'Admin\WebcomicController@store')->name('admin.webcomics.store');
    Route::get('/webcomics/{webcomic}/edit', 'Admin\WebcomicController@edit')->name('admin.webcomics.edit');
    Route::post('/webcomics/{webcomic}/edit', 'Admin\WebcomicController@update')->name('admin.webcomics.edit');
    Route::get('/webcomics/create', 'Admin\WebcomicController@create')->name('admin.webcomics.create');
    Route::post('/webcomics/{webcomic}/source', 'Admin\WebcomicSourceController@store')->name('admin.webcomics.source.store');
    Route::get('/webcomics/{webcomic}/addsource', 'Admin\WebcomicSourceController@create')->name('admin.webcomics.source.create');
    Route::get('webcomics/{webcomic}/s/{source}', 'Admin\WebcomicSourceController@edit')->name('admin.webcomics.source.edit');
    Route::get('webcomics/{webcomic}/', 'Admin\WebcomicSourceController@index')->name('admin.webcomics.source.index');

    Route::get('/users', 'Admin\UserController@index')->name('admin.user.index');
    Route::get('/users/{user}', 'Admin\UserController@show')->name('admin.user.show');

});
