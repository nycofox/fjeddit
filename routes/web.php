<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/', 'HomeController@index')->name('home');

Route::get('u/{user}', 'ProfileController@show')->name('profile');
Route::get('settings', 'UserSettingsController@show')->name('settings')->middleware('auth');
Route::post('settings/password', 'UserSettingsController@storePassword')->name('settings.storepassword')->middleware('auth');

Route::post('c', 'ChannelController@store')->name('channel.store')->middleware('auth');
Route::get('c/{channel}/create', 'ThreadController@create')->name('thread.create')->middleware('auth');
Route::post('c/{channel}/thread/store', 'ThreadController@store')->name('thread.store');
Route::get('c/{channel}/thread/{thread}', 'ThreadController@show')->name('thread.show');
Route::post('c/{channel}/thread/{thread}/report', 'ThreadController@report')->name('thread.report');
Route::post('c/{channel}/thread/{thread}/reply', 'ReplyController@store')->name('thread.reply');


//Route::pattern('channel', '(?i)channel(?-i)');
Route::get('c/{channel}', 'ChannelController@show')->name('channel.show');

Route::post('c/{channel}/subscribe', 'ChannelSubscriptionController@store')->name('channel.subscribe');
Route::post('c/{channel}/unsubscribe', 'ChannelSubscriptionController@destroy')->name('channel.unsubscribe');

require '_admin.php';