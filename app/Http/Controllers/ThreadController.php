<?php

namespace App\Http\Controllers;

use App\Models\Channel;
use App\Models\Reply;
use App\Models\Thread;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ThreadController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('show');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Channel $channel
     * @return void
     */
    public function create(Channel $channel)
    {
        return view('threads.create')->with(compact('channel'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Channel $channel
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Channel $channel, Request $request)
    {
        request()->validate([
            'title' => 'required|min:6',
            'channel_id' => ['required', Rule::exists('channels', 'id')],
        ]);

        $thread = Thread::create([
            'title' => $request->title,
            'body' => $request->body,
            'user_id' => auth()->id(),
            'channel_id' => $request->channel_id,
        ]);

        flash("You've added a new thread!");

        return redirect(route('thread.show', [$channel, $thread]));
    }

    /**
     * Display the specified resource.
     *
     * @param Channel $channel
     * @param  \App\Models\Thread $thread
     * @return \Illuminate\Http\Response
     */
    public function show(Channel $channel, Thread $thread)
    {
        $replies = Reply::with('user')->whereThreadId($thread->id)->latest()->paginate(20);

        $thread->increment('views');

        return view('threads.show')->with(compact('thread', 'replies'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Thread $thread
     * @return \Illuminate\Http\Response
     */
    public function edit(Channel $channel, Thread $thread)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Thread $thread
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Channel $channel, Thread $thread)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Thread $thread
     * @return \Illuminate\Http\Response
     */
    public function destroy(Channel $channel, Thread $thread)
    {
        //
    }
}
