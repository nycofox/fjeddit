<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserSettingsController extends Controller
{

    private $user;

    public function __construct()
    {
        $this->user = \Auth::user();
    }

    /**
     * Display the users settings page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        $user = $this->user;

        return view('settings.show')->with(compact('user'));
    }

    /**
     * Stores a new password for the signed in user. Validates the current one first
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storePassword()
    {
        request()->validate([
            'oldpassword' => 'required',
            'newpassword' => 'required|min:6|different:oldpassword|confirmed'
        ]);

        $user = \Auth::user();

        if (\Hash::check(request()->oldpassword, $user->password)) {
            $user->password = \Hash::make(request()->newpassword);
            $user->save();

//            flash('Password updated successfully!')->success();

            return redirect(route('settings'));
        }

        return redirect(route('settings'))
            ->withErrors(['message' => 'Failed to change password, make sure your current password is correct.']);
    }
}
