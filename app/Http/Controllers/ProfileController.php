<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function show(User $user)
    {
        $profileuser = $user;

        return view('profile.show')->with(compact('profileuser'));
    }
}
