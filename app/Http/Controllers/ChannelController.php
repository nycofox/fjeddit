<?php

namespace App\Http\Controllers;

use App\Models\Channel;
use App\Models\Thread;
use Illuminate\Http\Request;

class ChannelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store()
    {
        request()->validate([
            'name' => 'required',
        ]);

        $channel = Channel::create([
            'owner_id' => auth()->id(),
            'creator_id' => auth()->id(),
            'name' => request()->name,
            'description' => request()->description
        ]);

        return redirect(route('channel.show', $channel));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Channel $channel
     * @return \Illuminate\Http\Response
     */
    public function show(Channel $channel)
    {
//        $threads = $channel->threads;
        $threads = Thread::whereChannelId($channel->id)->with('user')->latest()->get();

        return view('channel.show')->with(compact('channel', 'threads'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Channel $channel
     * @return \Illuminate\Http\Response
     */
    public function edit(Channel $channel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Channel $channel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Channel $channel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Channel $channel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Channel $channel)
    {
        //
    }
}
