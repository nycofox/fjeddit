<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $threads = \App\Models\Thread::with('channel')
            ->with('user')
            ->withCount('replies')
            ->latest()
            ->paginate(10);

        return view('index')->with(compact('threads'));
    }
}
