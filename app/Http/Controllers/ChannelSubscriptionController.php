<?php

namespace App\Http\Controllers;

use App\Models\Channel;
use Illuminate\Http\Request;

class ChannelSubscriptionController extends Controller
{

    /**
     * Subscribe a user to a channel
     *
     * @param Channel $channel
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Channel $channel)
    {
        $channel->subscribe();

        flash("You successfully subscribed to " . $channel->name)->success();

        return redirect()->back();
    }

    /**
     * Unsubscribe a user from a channel
     *
     * @param Channel $channel
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Channel $channel)
    {
        $channel->unsubscribe();

        flash("You successfully unsubscribed from " . $channel->name)->success();

        return redirect()->back();
    }
}
