<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $count = [
            'users' => User::count(),
            'newusers' => User::where('created_at', '>', now()->subDays(7))->count(),
        ];

        return view('admin.index')->with(compact('count'));
    }
}
