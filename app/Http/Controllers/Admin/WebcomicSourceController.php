<?php

namespace App\Http\Controllers\Admin;

use App\Models\Webcomic;
use App\Models\WebcomicSource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class WebcomicSourceController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Webcomic $webcomic
     * @return \Illuminate\Http\Response
     */
    public function index(Webcomic $webcomic)
    {
        $sources = WebcomicSource::whereWebcomicId($webcomic->id)->get();

        return view('admin.webcomics.sources.index')->with(compact('webcomic', 'sources'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Webcomic $webcomic
     * @return \Illuminate\Http\Response
     */
    public function create(Webcomic $webcomic)
    {
        return view('admin.webcomics.sources.create')->with(compact('webcomic'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Webcomic $webcomic
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Webcomic $webcomic)
    {
        request()->validate([
            'slug' => 'required|min:2|unique:webcomic_sources',
            'homepage' => 'required|url',
            'type' => Rule::in(['s', 'g']),
            'searchpage' => 'nullable|url'
        ]);

        $source = WebcomicSource::create(
            array_merge(
                request()->toArray(),
                ['webcomic_id' => $webcomic->id]
            )
        );

        redirect(route('admin.webcomics.source.edit', [$webcomic, $source]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
