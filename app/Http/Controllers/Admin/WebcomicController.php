<?php

namespace App\Http\Controllers\Admin;

use App\Models\Webcomic;
use App\Models\WebcomicSource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WebcomicController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showDashboard()
    {
        $count = [
            'webcomics' => Webcomic::count(),
            'sources' => WebcomicSource::count(),
            'strips_last_week' => 0,
            
        ];


        return view('admin.webcomics.dashboard')->with(compact('count'));
    }

    /**
     * List all webcomics
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $webcomics = Webcomic::withCount('sources')->get();

        return view('admin.webcomics.index')->with(compact('webcomics'));
    }

    /**
     * Display the form for adding a new webcomic
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.webcomics.create');
    }

    /**
     * Store a new webcomic in the database
     */
    public function store()
    {
        Webcomic::create(request()->validate([
            'name' => 'required|min:2|unique:webcomics',
            'slug' => 'required|min:2|unique:webcomics',
            'author' => '',
        ]));

        return redirect(route('admin.webcomics.index'));
    }

    /**
     * Display the form for editing an existing webcomic
     *
     * @param Webcomic $webcomic
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Webcomic $webcomic)
    {
        return view('admin.webcomics.edit')->with(compact('webcomic'));
    }

    /**
     * Update an existing webcomic in the database
     *
     * @param Webcomic $webcomic
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Webcomic $webcomic)
    {
        $webcomic->update(request()->validate([
            'name' => 'min:2|unique:webcomics,name,' . $webcomic->id,
            'slug' => 'min:2|unique:webcomics,slug,' . $webcomic->id,
            'author' => ''
        ]));

        return redirect(route('admin.webcomics.index', $webcomic));
    }

}
