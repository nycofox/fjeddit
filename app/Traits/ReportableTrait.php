<?php

namespace App\Traits;

use Illuminate\Support\Facades\Request;

trait ReportableTrait
{
    /**
     * @param Request $request
     */
    public function report(Request $request)
    {

    }
}
