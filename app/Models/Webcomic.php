<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Webcomic extends Model
{

    /**
     * Disable Mass Assignment Protection
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Cast columns as date
     *
     * @var array
     */
    protected $dates = ['last_scraped_at'];

    /**
     * Set route key to slug
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * A Webcomic can have many sources
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sources()
    {
        return $this->hasMany(WebcomicSource::class);
    }
}
