<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{

    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'name';
    }

    /**
     * A Channel can have many Threads
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function threads()
    {
        return $this->hasMany(Thread::class);
    }

    /**
     * A Channel can have many Subscribers
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subscriptions()
    {
        return $this->hasMany(ChannelSubscription::class);
    }

    /**
     * Subscribe a user to the Channel
     *
     * @param null $userId
     * @return $this
     */
    public function subscribe($userId = null)
    {
        $this->subscriptions()->create(['user_id' => $userId ?: auth()->id()]);

        return $this;
    }

    /**
     * Unsubscribe a user from a Channel
     *
     * @param null $userId
     * @return Channel
     */
    public function unsubscribe($userId = null)
    {
        $this->subscriptions()
            ->where('user_id', $userId ?: auth()->id())
            ->delete();

        return $this;
    }

    /**
     * @param $userId
     * @return bool
     */
    public function isSubscribed($userId = null)
    {
        return $this->subscriptions()
                ->where('user_id', $userId ?: auth()->id())
                ->exists();
    }

    /**
     * A channel has a creator
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    /**
     * A channel has a creator
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id', 'id');
    }
}
