<?php

namespace App\Models;

use App\Traits\RecordsActivity;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Cache;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, RecordsActivity;

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'name';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Cast attributes as Carbon dates
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'email_verified_at'];

    /**
     * Boot the function and create
     */
    public static function boot()
    {
        parent::boot();

        static::created(function ($user) {
            $user->albums()->create([
                'type' => 'p',
                'system' => true,
                'title' => 'Profile pictures'
            ]);
        });
    }

    /**
     * A User can have many Threads
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function threads()
    {
        return $this->hasMany(Thread::class);
    }

    /**
     * A user can have many albums
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function albums()
    {
        return $this->hasMany(Album::class);
    }

    /**
     * A user can have many roles
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
    }

    /**
     * Does the user have a particular role?
     *
     * @param $name
     * @return bool
     */
    public function hasRole($name)
    {
        foreach ($this->roles as $role) {
            if ($role->role == $name) return true;
        }

        return false;
    }

    /**
     * Assign a role to the user
     *
     * @param $role
     * @return mixed
     */
    public function assignRole($role)
    {
        $role = Role::firstOrCreate(['role' => $role]);

        $this->roles()->attach($role->id);
    }

    /**
     * Remove a role from a user
     *
     * @param $role
     * @return mixed
     */
    public function removeRole($role)
    {
        $role = Role::find(['role' => $role])->first();

        return $this->roles()->detach($role);
    }

    /**
     * Returns true if the user is online
     * Value is stored in cache
     *
     * @return boolean
     */
    public function isOnline()
    {
        return Cache::has('user-is-online-' . $this->id);
    }

    /**
     * Returns the timestamp for when the user was last active
     * False if never
     *
     * @return bool|mixed
     */
    public function lastActiveTimeStamp()
    {
        if (Cache::has('user-last-activity-' . $this->id)) {
            $time = Cache::get('user-last-activity-' . $this->id);

            return $time->timestamp;
        }

        return false;
    }

    /**
     * Returns a human readable time for when the user was last active
     *
     * @return bool|mixed
     */
    public function lastActive()
    {
        if ($this->lastActiveTimeStamp()) {
            $time = Cache::get('user-last-activity-' . $this->id);

            return $time->diffForHumans();
        }

        return false;
    }

    /**
     * Increase a users karma score
     * Defaults to 1
     *
     * @param int $amount
     */
    public function giveKarma($amount = 1)
    {
        $this->increment('karma', $amount);
    }

    /**
     * Decrease a users karma score
     *
     * @param int $amount
     */
    public function removeKarma($amount = 1)
    {
        $this->decrement('karma', $amount);
    }

    /**
     * Relation to channel subscriptions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subscriptions()
    {
        return $this->hasMany(ChannelSubscription::class);
    }
}
