<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WebcomicSource extends Model
{
    /**
     * Disable Mass Assignment Protection
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Cast columns as date
     *
     * @var array
     */
    protected $dates = ['last_scrape_attempt_at', 'last_scraped_at'];

    /**
     * A source belongs to a Webcomic
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function webcomic()
    {
        return $this->belongsTo(Webcomic::class);
    }
}
