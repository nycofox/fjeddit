@extends('layouts.app')

@section('content')
    <div class="lg:flex">
        <div class="w-3/4 mb-4">
            @forelse($threads as $thread)
                @include('threads._front')
            @empty
                <div>Much empty, such void.</div>
            @endforelse
            {{ $threads->links() }}
        </div>
        <div class="w-1/4">
            @include('layouts._sidebar')
        </div>
    </div>
@endsection
