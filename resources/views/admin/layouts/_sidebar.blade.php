<div class="bg-black shadow-lg h-16 fixed pin-b mt-12 md:relative md:h-screen z-10 w-full md:w-48">

    <div class="md:mt-12 md:w-48 md:fixed md:pin-l md:pin-t content-center md:content-start text-left justify-between">
        <ul class="list-reset flex flex-row md:flex-col py-0 md:py-3 px-1 md:px-2 text-center md:text-left">
            <li class="mr-3 flex-1">
                <a href="{{ route('admin.dashboard') }}"
                   class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-white border-b-2 border-grey-darkest hover:border-pink">
                    <i class="fas fa-tasks pr-0 md:pr-3"></i><span
                            class="pb-1 md:pb-0 text-xs md:text-base text-grey-dark md:text-grey-light block md:inline-block">Dashboard</span>
                </a>
            </li>
            <li class="mr-3 flex-1">
                <a href="{{ route('admin.user.index') }}"
                   class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-white border-b-2 border-grey-darkest hover:border-purple">
                    <i class="fa fa-users pr-0 md:pr-3"></i><span
                            class="pb-1 md:pb-0 text-xs md:text-base text-grey-dark md:text-grey-light block md:inline-block">Users</span>
                </a>
            </li>
            <li class="mr-3 flex-1">
                <a href="{{ route('admin.webcomics.dashboard') }}"
                   class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-white border-b-2 border-blue-dark">
                    <i class="fas fa-chart-area pr-0 md:pr-3 text-blue-dark"></i><span
                            class="pb-1 md:pb-0 text-xs md:text-base text-white md:text-white block md:inline-block">Webcomics</span>
                </a>
            </li>
            <li class="mr-3 flex-1">
                <a href="#"
                   class="block py-1 md:py-3 pl-0 md:pl-1 align-middle text-white no-underline hover:text-white border-b-2 border-grey-darkest hover:border-red">
                    <i class="fa fa-wallet pr-0 md:pr-3"></i><span
                            class="pb-1 md:pb-0 text-xs md:text-base text-grey-dark md:text-grey-light block md:inline-block">Other...</span>
                </a>
            </li>
        </ul>
    </div>

</div>