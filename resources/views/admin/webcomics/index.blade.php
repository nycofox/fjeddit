@extends('admin.layouts.app')

@section('content')


    <div class="main-content flex-1 bg-grey-lightest mt-12 md:mt-2 pb-24 md:pb-5">

        <div class="bg-blue-darker p-2 shadow text-xl text-white">
            <h3 class="pl-2">List of webcomics</h3>
        </div>

        <div class="flex flex-wrap flex-fill p-2">
            <table class="table-fixed" id="webcomics">
                <thead>
                <tr>
                    <th>Webcomic</th>
                    <th>Author</th>
                    <th># of sources</th>
                    <th>Last scraped</th>
                    <th>Last scraped (hidden)</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($webcomics as $webcomic)
                    <tr>
                        <td>
                            <a href="{{ route('admin.webcomics.source.index', $webcomic) }}">{{ $webcomic->name }}</a>
                        </td>
                        <td>{{ $webcomic->author }}</td>
                        <td>{{ $webcomic->sources_count }}</td>
                        <td>{{ $webcomic->last_scraped_at ? $webcomic->last_scraped_at->diffForHumans() : 'Never' }}</td>
                        <td>{{ $webcomic->last_scraped_at }}</td>
                        <td>
                            <a href="{{ route('admin.webcomics.edit', $webcomic) }}">Edit</a> |
                            <a href="{{ route('admin.webcomics.source.create', [$webcomic]) }}">Add source</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <a href="{{ route('admin.webcomics.create') }}">Add a new webcomic</a>
        </div>
    </div>
@endsection

@push('pagescripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#webcomics').DataTable({
                "columnDefs": [
                    {"searchable": false, "targets": [2, 3, 4, 5]},
                    {"visible": false, "targets": [4]},
                    {"orderData": [4], "targets": [3]}
                ]
            });
        });
    </script>
@endpush
