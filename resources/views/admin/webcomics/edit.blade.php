@extends('admin.layouts.app')

@section('content')

    <div class="main-content flex-1 bg-grey-lightest mt-12 md:mt-2 pb-24 md:pb-5">

        <div class="bg-blue-darker p-2 shadow text-xl text-white">
            <h3 class="pl-2">{{ $webcomic->name }} - Edit</h3>
        </div>

        <div class="flex flex-wrap flex-fill m-2">
            <form action="{{ route('admin.webcomics.edit', $webcomic) }}" method="post">
                @include('admin.webcomics._form_webcomic', [
                    'webcomic' => $webcomic,
                    'buttonText' => 'Update WebComic'
                ])
            </form>
        </div>
    </div>
@endsection
