<div class="w-full md:w-1/2 xl:w-1/3 p-3">
    <!--Metric Card-->
    <div class="bg-blue-lightest border-b-4 border-blue rounded-lg shadow-lg p-5">
        <div class="flex flex-row items-center">
            <div class="flex-shrink pr-4">
                <div class="rounded-full p-5 bg-blue-dark"><i class="fas fa-server fa-2x fa-inverse"></i>
                </div>
            </div>
            <div class="flex-1 text-right md:text-center">
                <h5 class="uppercase text-grey-dark">Number of sources</h5>
                <h3 class="text-3xl">{{ $count['sources'] }}</h3>
            </div>
        </div>
    </div>

    <!--/Metric Card-->
</div>
