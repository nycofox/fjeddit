@extends('admin.layouts.app')

@section('content')
    <div class="main-content flex-1 bg-grey-lightest mt-12 md:mt-2 pb-24 md:pb-5">

        <div class="bg-blue-darker p-2 shadow text-xl text-white">
            <h3 class="pl-2">Webcomics</h3>
        </div>

        <div class="flex flex-wrap">
            @include('admin.webcomics.snippets.total_webcomics')

            @include('admin.webcomics.snippets.total_sources')
        </div>
    </div>
@endsection
