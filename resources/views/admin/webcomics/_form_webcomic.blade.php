@csrf

<div class="field mb-6">
    <label class="label text-sm mb-2 block" for="name">Name</label>

    <div class="control">
        <input
                type="text"
                class="input bg-transparent border border-grey-light rounded p-2 text-xs w-full"
                name="name"
                placeholder="Name of webcomic"
                required
                value="{{ old('name', $webcomic->name) }}">
    </div>
</div>

<div class="field mb-6">
    <label class="label text-sm mb-2 block" for="slug">Slug</label>

    <div class="control">
        <input
                type="text"
                class="input bg-transparent border border-grey-light rounded p-2 text-xs w-full"
                name="slug"
                placeholder="URL friendly slug"
                required
                value="{{ old('slug', $webcomic->slug) }}">
    </div>
</div>

<div class="field mb-6">
    <label class="label text-sm mb-2 block" for="author">Author</label>

    <div class="control">
        <input
                type="text"
                class="input bg-transparent border border-grey-light rounded p-2 text-xs w-full"
                name="author"
                placeholder="Name of author (optional)"
                required
                value="{{ old('author', $webcomic->author) }}">
    </div>
</div>

<div class="field">
    <div class="control">
        <button type="submit" class="button is-link mr-2">{{ $buttonText }}</button>
    </div>
</div>