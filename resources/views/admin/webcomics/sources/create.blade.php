@extends('admin.layouts.app')

@section('content')

    <div class="main-content flex-1 bg-grey-lightest mt-12 md:mt-2 pb-24 md:pb-5">

        <div class="bg-blue-darker p-2 shadow text-xl text-white">
            <h3 class="pl-2">Add a new source to {{ $webcomic->name }}</h3>
        </div>

        <div class="flex flex-wrap flex-fill m-2">
            <form class="w-full max-w-xs" action="{{ route('admin.webcomics.source.store', [$webcomic]) }}"
                  method="post">
                @include('admin.webcomics.sources._form_source', [
                    'source' => new \App\Models\WebcomicSource,
                    'buttonText' => 'Create new source for ' . $webcomic->name
                ])
            </form>
        </div>
    </div>
@endsection
