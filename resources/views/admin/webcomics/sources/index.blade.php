@extends('admin.layouts.app')

@section('content')
    <div class="main-content flex-1 bg-grey-lightest mt-12 md:mt-2 pb-24 md:pb-5">

        <div class="bg-blue-darker p-2 shadow text-xl text-white">
            <h3 class="pl-2">List of sources for {{ $webcomic->name }}</h3>
        </div>

        <div class="flex flex-wrap flex-fill p-2">
            <table class="table-fixed" id="sources">
                <thead>
                <tr>
                    <th>Source</th>
                    <th>Homepage</th>
                    <th>Type</th>
                    <th># of strips</th>
                    <th>Last scraped</th>
                    <th>Last scraped (hidden)</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($sources as $source)
                    <tr>
                        <td>
                            <a href="#">{{ $source->name }}</a>
                        </td>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                        <td>{{ $source->last_scraped_at ? $source->last_scraped_at->diffForHumans() : 'Never' }}</td>
                        <td>{{ $source->last_scraped_at }}</td>
                        <td>...</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <a href="{{ route('admin.webcomics.source.create', [$webcomic]) }}">Add a new source</a>
        </div>
    </div>
@endsection

@push('pagescripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sources').DataTable({
                "columnDefs": [
                    {"searchable": false, "targets": [2, 3, 4, 5, 6]},
                    {"visible": false, "targets": [5]},
                    {"orderData": [5], "targets": [4]}
                ]
            });
        });
    </script>
@endpush
