@extends('admin.layouts.app')

@section('content')


    <div class="main-content flex-1 bg-grey-lightest mt-12 md:mt-2 pb-24 md:pb-5">

        <div class="bg-blue-darker p-2 shadow text-xl text-white">
            <h3 class="pl-2">List of users</h3>
        </div>

        <div class="flex flex-wrap flex-fill p-2">
            <table class="table-fixed" id="users">
                <thead>
                <tr>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Date joined</th>
                    <th>Date joined sortable (hidden)</th>
                    <th>Last seen</th>
                    <th>Last seen sortable (hidden)</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>
                            <a href="{{ route('admin.user.show', $user) }}">{{ $user->name }}</a>
                            @if($user->isOnline())
                                <small class="bg-green text-white p-1 rounded">ONLINE</small>
                            @endif
                        </td>
                        <td>{{ $user->email }}</td>
                        <td title="{{ $user->created_at }}">{{ $user->created_at->diffForHumans() }}</td>
                        <td>{{ $user->created_at->timestamp }}</td>
                        <td>{{ $user->lastActive() }}</td>
                        <td>{{ $user->lastActiveTimestamp() }}</td>
                        <td>STATUS</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('pagescripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#users').DataTable({
                "pageLength": 20,
                "columnDefs": [
                    {"searchable": false, "targets": [2, 3, 4, 5, 6]},
                    {"visible": false, "targets": [3, 5]},
                    {"orderData": [3,5], "targets": [2,4]},
                ]
            });
        });
    </script>
@endpush
