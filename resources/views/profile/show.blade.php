@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Profile page</div>

                <div class="card-body">
                    <h1>{{ $profileuser->name }}</h1>
                </div>
            </div>
        </div>
    </div>
@endsection
