<h4><strong>Registration</strong></h4>
<p>Anyone 18 years of age or above can register an account, provided they have not been previously banned from the
    site.</p>

<h4>Termination</h4>
<p>You can cancel your account at any time. {{ config('app.name') }} reserves the right to delete any account at any
    time for any reason.</p>

<h4>Intellectual Property Rights</h4>
<p>You will retain all ownership of content posted to this site. We will not use any content outside the site, for any
    reason.</p>

<h4>Privacy</h4>
<p>We will never share your content outside the site. You can choose who will see the content that you post. Be aware
    that admins have access to see all content. We will also not share your personal information to anyone. This site is
    (for the time being) completely ad-free.</p>

<h4>Allowed content</h4>
<p>Most content will be allowed within the rules of their respective channels. The following content is universally
    prohibited:
<ul>
    <li>Anything illegal</li>
    <li>Involuntary pornography, or pornography involving minors</li>
    <li>Anything that encourages violence or harassment</li>
    <li>Threats, bullying and similar</li>
    <li>Anything containing personal information</li>
    <li>Impersonating someone else in a misleading or deceptive matter</li>
    <li>Spam, boy do we hate it</li>
</ul>
</p>
<p>If your content is considered unfit for a work environment (NSFW, Not Safe For Work) it should be tagged as such.
    Certain channels may auto-tag content as NSFW.</p>

<h4>Cookies and such</h4>
<p>We use cookies to track your online status. We do not use cookies for any other reason, including marketing.</p>

<h4>Boring legal stuff</h4>
<p>You use {{ config('app.name') }} at your own risk, ok? Don't come whining if someone hacks your account and kills
    your pet goldfish.</p>
