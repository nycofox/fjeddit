@foreach (session('flash_notification', collect())->toArray() as $message)
    @if ($message['overlay'])
        @include('flash::modal', [
            'modalClass' => 'flash-modal',
            'title'      => $message['title'],
            'body'       => $message['message']
        ])
    @else
        <?php

        if ($message['level'] == 'success') {
            $color = 'green';
        } elseif ($message['level'] == 'warning') {
            $color = 'yellow';
        } elseif ($message['level'] == 'error') {
            $color = 'red';
        } else {
            $color = 'blue';
        }

        ?>
        <div class="bg-{{ $color }}-lightest border-l-4 border-{{ $color }} text-{{ $color }}-dark p-2 mb-2"
             role="alert">
            @if ($message['important'])
                <button type="button"
                        class="close"
                        data-dismiss="alert"
                        aria-hidden="true"
                >&times;
                </button>
            @endif

            <p>{!! $message['message'] !!}</p>
        </div>
    @endif
@endforeach

{{ session()->forget('flash_notification') }}
