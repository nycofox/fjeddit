@extends('layouts.app')

@section('content')
    <main>
        <div class="lg:flex -mx-3">
            <div class="lg:w-3/4 px-3 mb-6">
                <div class="mb-8">
                    <h2 class="text-lg font-strong mb-3">{{ $channel->name }}</h2>
                </div>
                @foreach($threads as $thread)
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ route('thread.show', [$thread->channel, $thread]) }}">
                                {{ $thread->title }}
                            </a>
                            by
                            <a href="{{ route('profile', $thread->user) }}">{{ $thread->user->name }}</a>
                        </div>

                        <div class="card-body">
                            Nothing...
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="lg:w-1/4 px-3 lg:py-8">
                @include('channel._sidebar')
            </div>
        </div>
    </main>
@endsection
