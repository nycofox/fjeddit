<div class="col-md-4">
    <div class="card bg-light">
        <div class="card-header mb-2">
            Welcome to
            <a href="{{ route('channel.show', $channel) }}" class="font-bold">c/{{ $channel->name }}</a>
        </div>
        <div class="card-body">
            <p class="card-text text-sm mb-4">{{ $channel->description }}</p>
            <div>
                @guest
                    <p class="text-sm mb-2">You need to log in to subscribe.</p>
                @else
                    @if($channel->isSubscribed())
                        <form method="POST" action="{{ route('channel.unsubscribe', $channel) }}">
                            @csrf
                            <button type="submit"
                                    class="bg-red-light hover:bg-red p-1 rounded mb-4 text-sm text-white"
                                    title="Click here to unsubscribe from {{ $channel->name }}">
                                Unsubscribe
                            </button>
                        </form>
                    @else
                        <form method="POST" action="{{ route('channel.subscribe', $channel) }}">
                            @csrf
                            <button type="submit"
                                    class="bg-green-light hover:bg-green p-1 rounded mb-2 text-sm text-white"
                                    title="You are currently not subscribed to {{ $channel->name }}, click here to subscribe">
                                Subscribe
                            </button>
                        </form>
                    @endif
                @endguest
                <p class="text-xs mb-4">XXX people are subscribing to {{ $channel->name }}</p>
            </div>
            <a class="bg-blue-light hover:bg-blue pt-1 pb-1 pr-3 pl-3 rounded mb-2 text-sm text-white no-underline"
               href="{{ route('thread.create', $channel) }}">Create new post</a>
        </div>
    </div>
</div>
