@if ($paginator->hasPages())
    <ul class="flex list-reset border border-grey-light rounded w-auto font-sans">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="block text-grey border-r border-grey-light px-3 py-2">Previous</li>
        @else
            <li>
                <a class="block hover:text-white hover:bg-blue no-underline text-blue border-r border-grey-light px-3 py-2"
                   href="{{ $paginator->previousPageUrl() }}">Previous</a>
            </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <span class="border-t border-b border-l border-brand-light px-3 py-2 cursor-not-allowed no-underline">{{ $element }}</span>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="block text-white bg-blue border-r border-blue px-3 py-2">{{ $page }}</li>
                    @else
                        <li>
                            <a class="block hover:text-white hover:bg-blue no-underline text-blue border-r border-grey-light px-3 py-2"
                               href="{{ $url }}">{{ $page  }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li><a class="block hover:text-white hover:bg-blue no-underline text-blue px-3 py-2"
                   href="{{ $paginator->nextPageUrl() }}">Next</a></li>
        @else
            <li class="block no-underline text-grey px-3 py-2">Next</li>
        @endif
    </ul>
@endif
