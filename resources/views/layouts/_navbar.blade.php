<nav class="bg-white">
    <div class="container mx-auto">
        <div class="flex justify-between items-center p-2">
            <h1 class="text-xl">
                <a class="no-underline text-grey-dark" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
            </h1>

            <div>
                <!-- Right Side Of Navbar -->
                <ul class="list-reset flex justify-between flex-1 md:flex-none items-center">
                    <!-- Authentication Links -->
                    @guest
                        <li class="flex-1 md:flex-none md:mr-3">
                            <a class="inline-block py-2 text-grey-dark px-4 no-underline"
                               href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        <li class="flex-1 md:flex-none md:mr-3">
                            <a class="inline-block py-2 text-grey-dark px-4 no-underline"
                               href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @else
                        <li class="flex-1 md:flex-none md:mr-3">
                            <a class="inline-block py-2 px-4 no-underline" href="#" title="You have XXX new messages">
                                <i class="fa fa-envelope fa-fw"></i>
                            </a>
                        </li>
                        <li class="flex-1 md:flex-none md:mr-3">

                            <div class="relative inline-block">
                                <button onclick="toggleDD('myDropdown')"
                                        class="drop-button focus:outline-none"><span class="pr-2"><i
                                                class="em em-robot_face"></i></span> {{ Auth::user()->name }}
                                    <svg class="h-3 fill-current" xmlns="http://www.w3.org/2000/svg"
                                         viewBox="0 0 20 20">
                                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                                    </svg>
                                </button>
                                <div id="myDropdown"
                                     class="dropdownlist absolute bg-grey pin-r mt-3 p-3 overflow-auto z-30 invisible">
                                    <input type="text" class="drop-search p-2 text-grey-dark" placeholder="Search.."
                                           id="myInput" onkeyup="filterDD('myDropdown','myInput')">
                                    <a href="{{ route('profile', Auth::user()) }}"
                                       class="p-2 hover:bg-grey-darkest text-white text-sm no-underline hover:no-underline block">
                                        <i class="fa fa-user fa-fw"></i> Profile</a>
                                    <a href="#"
                                       class="p-2 hover:bg-grey-darkest text-white text-sm no-underline hover:no-underline block">
                                        <i class="fa fa-cog fa-fw"></i> Settings</a>
                                    <div class="border border-grey-darkest"></div>
                                    <a href="{{ route('logout') }}" class="p-2 hover:bg-grey-darkest text-white
                                    text-sm no-underline hover:no-underline block" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                        <i class="fas fa-sign-out-alt fa-fw"></i> {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        @csrf
                                    </form>
                                    @if(Auth::user()->hasRole('admin'))
                                        <div class="border border-grey-darkest"></div>
                                        <a href="{{ route('admin.dashboard') }}"
                                           class="p-2 hover:bg-grey-darkest text-white text-sm no-underline hover:no-underline block">
                                            <i class="fa fa-user fa-fw"></i> Admin</a>
                                    @endif
                                </div>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </div>
</nav>

@push('pagescripts')
    <script>
        /*Toggle dropdown list*/
        function toggleDD(myDropMenu) {
            document.getElementById(myDropMenu).classList.toggle("invisible");
        }

        /*Filter dropdown options*/
        function filterDD(myDropMenu, myDropMenuSearch) {
            var input, filter, ul, li, a, i;
            input = document.getElementById(myDropMenuSearch);
            filter = input.value.toUpperCase();
            div = document.getElementById(myDropMenu);
            a = div.getElementsByTagName("a");
            for (i = 0; i < a.length; i++) {
                if (a[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
                    a[i].style.display = "";
                } else {
                    a[i].style.display = "none";
                }
            }
        }

        // Close the dropdown menu if the user clicks outside of it
        window.onclick = function (event) {
            if (!event.target.matches('.drop-button') && !event.target.matches('.drop-search')) {

                var dropdowns = document.getElementsByClassName("dropdownlist");
                for (var i = 0; i < dropdowns.length; i++) {
                    var openDropdown = dropdowns[i];
                    if (!openDropdown.classList.contains('invisible')) {
                        openDropdown.classList.add('invisible');
                    }
                }
            }
        }
    </script>
@endpush