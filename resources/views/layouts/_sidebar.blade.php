@guest
    @include('layouts.sidebars.login')
@else
    <div class="card ml-6">
        <p class="mb-5">Welcome {{ Auth::user()->name }}!</p>
    </div>
@endguest
