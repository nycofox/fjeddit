@auth
    @if(!Auth::user()->email_verified_at)
        <div class="flex bg-yellow-lighter mb-4 rounded">
            <div class="w-auto text-grey-darker items-center p-4">
                <p class="leading-tight">
                    You have not verified your email. Click <a href="{{ route('verification.resend') }}">here</a> to
                    resend verification link and check your email.
                </p>
            </div>
        </div>

    @endif
@endauth
