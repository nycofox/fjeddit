@if ($errors->any())
    <div class="bg-red-lightest border-l-4 border-red text-red-dark p-2 mb-2" role="alert">
        <p class="font-bold mb-2">Somebody did an oopsie!</p>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif