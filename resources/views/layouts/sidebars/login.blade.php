<div class="card ml-6">
    <h3>Sign in to participate.</h3>
    <form class="pt-6 pb-2 my-2" method="POST" action="{{ route('login') }}">
        @csrf
        <div class="mb-4">
            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker"
                   id="email" name="email" type="text" placeholder="Email Address">
        </div>
        <div class="mb-4">
            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker mb-3"
                   id="password" name="password" type="password" placeholder="Password">
        </div>
        <div class="mb-4">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" name="remember"
                       id="remember" {{ old('remember') ? 'checked' : '' }}>

                <label class="form-check-label" for="remember">
                    {{ __('Remember Me') }}
                </label>
            </div>
        </div>
        <div class="block md:flex items-center justify-between">
            <div>
                <button class="bg-green hover:bg-green-dark text-white font-bold py-2 px-4 rounded border-b-4 border-green-darkest"
                        type="submit">
                    Sign In
                </button>
            </div>

            <div class="mt-4 md:mt-0">
                <a href="#" class="text-green no-underline">Forget Password?</a><br>
                <a href="#" class="text-green no-underline">Register a new account</a><br>
            </div>
        </div>
    </form>
</div>
