<div class="card">
    <div class="card-body">
        <p class="mb-2">
            <a href="{{ route('profile', $reply->user) }}" class="no-underline text-sm">{{ $reply->user->name }}</a>
            <span class="text-xs">
                replied <a title="{{ $reply->created_at }}">{{ $reply->created_at->diffForHumans() }}</a>:
            </span>
        </p>
        <p class="ml-2 mr-2">{{ $reply->body }}</p>
    </div>
</div>