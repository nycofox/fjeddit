<div class="card">
    <div><a href="{{ route('thread.show', [$thread->channel, $thread]) }}" class="no-underline text-blue-darker">
            {{ $thread->title }}
        </a>
        <a href="#" class="no-underline text-xs text-grey-dark">(domain.com)</a>
    </div>
    <div class="text-grey-dark text-xs">
        submitted {{ $thread->created_at->diffForHumans() }} by
        <a href="{{ route('profile', $thread->user) }}"
           class="no-underline text-blue-dark">{{ $thread->user->name }}</a>
        in
        <a href="{{ route('channel.show', $thread->channel) }}"
           class="no-underline text-blue-dark">c/{{ $thread->channel->name }}</a>
    </div>
    <div>
        <a href="{{ route('thread.show', [$thread->channel, $thread]) }}" class="no-underline text-blue-dark text-sm">
            {{ $thread->replies_count }} comments</a>
    </div>

</div>