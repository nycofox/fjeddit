@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-lg mb-4"><span class="font-bold">{{ $thread->title }}</span>
                    <span class="text-sm">
                        by
                        <a href="{{ route('profile', $thread->user) }}" class="no-underline">{{ $thread->user->name }}</a>
                        submitted
                        <span title="{{ $thread->created_at }}">{{ $thread->created_at->diffForHumans() }}</span>
                    </span>
                </div>
                <div class="bg-grey-lightest shadow border border-grey rounded p-2">
                    {{ $thread->body }}
                </div>

                <div class="card-footer text-sm mt-2">
                    Showing {{ $replies->count() }} replies...
                </div>
            </div>

        </div>
    </div>

    <div class="card">
        <form action="{{ route('thread.reply', [$thread->channel, $thread]) }}" method="post">

            @include('threads._form_reply', [
                            'reply' => new \App\Models\Reply,
                            'buttonText' => 'Submit Reply'
                        ])
        </form>
    </div>

    @foreach($replies as $reply)
        @include('threads._reply')
    @endforeach

    {{ $replies->links() }}
@endsection
