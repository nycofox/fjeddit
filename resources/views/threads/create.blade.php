@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <h2>Create a new post in c/{{ $channel->name }}</h2>
    </div>

    <div>
        <form action="{{ route('thread.store', $channel) }}" method="POST">
            @csrf
            <input type="text" name="title" id="title">
            <textarea name="body"></textarea>

            <button type="submit">Submit post</button>
        </form>
    </div>

@endsection
