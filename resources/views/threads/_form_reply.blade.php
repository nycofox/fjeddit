@csrf
<textarea
        name="body"
        rows="3"
        class="card w-full mb-4 bg-grey-lightest"
        placeholder="Please be civil..."
        style="resize:none"
>{{ $reply->body }}</textarea>
<button type="submit" class="button is-link mr-2 rounded-sm">{{ $buttonText }}</button>
