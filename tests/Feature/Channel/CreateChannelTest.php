<?php

namespace Tests\Feature\Channel;

use App\Models\Channel;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateChannelTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function a_registered_user_can_create_a_new_channel()
    {
        $this->signIn()->withExceptionHandling();

        $channel = make(Channel::class);

        $this->post(route('channel.store'), $channel->toArray())
            ->assertRedirect(route('channel.show', $channel));

        $this->assertCount(1, Channel::all());
    }

    /** @test */
    function guests_may_not_create_channels()
    {
        $this->withExceptionHandling();

        $channel = make(Channel::class);

        $this->post(route('channel.store'), $channel->toArray())
            ->assertRedirect(route('login'));
    }

}
