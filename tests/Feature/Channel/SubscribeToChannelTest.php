<?php

namespace Tests\Feature\Channel;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SubscribeToChannelTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function a_user_can_subscribe_to_a_channel()
    {
        $this->signIn();

        $channel = create(\App\Models\Channel::class);

        $this->post(route('channel.subscribe', $channel));

        $this->assertCount(1, $channel->fresh()->subscriptions);
    }

    /** @test */
    function a_user_can_unsubscribe_from_a_channel()
    {
        $this->signIn();

        $channel = create(\App\Models\Channel::class);

        $this->post(route('channel.subscribe', $channel));

        $this->assertCount(1, $channel->fresh()->subscriptions);

        $this->post(route('channel.unsubscribe', $channel));

        $this->assertCount(0, $channel->fresh()->subscriptions);
    }
}
