<?php

namespace Tests\Feature\Channel;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ChannelViewTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function a_channel_has_its_own_page()
    {
        $channel = create(\App\Models\Channel::class);

        $this->get(route('channel.show', $channel))
            ->assertStatus(200)
            ->assertSee($channel->name);
    }

    /** @test */
    function a_channel_page_lists_threads()
    {
        $channel = create(\App\Models\Channel::class);

        $thread = create(\App\Models\Thread::class, ['channel_id' => $channel->id]);

        $this->get(route('channel.show', $channel))
            ->assertSee($thread->title);
    }

    /** @test */
    function a_channel_page_does_not_list_threads_beloning_to_another_channel()
    {
        $channel = create(\App\Models\Channel::class);

        $thread = create(\App\Models\Thread::class);

        $this->get(route('channel.show', $channel))
            ->assertDontSee($thread->title);
    }

    /** @test */
    function a_channel_has_a_sidebar_with_channel_information()
    {
        $channel = create(\App\Models\Channel::class);

        $this->get(route('channel.show', $channel))
            ->assertSee($channel->description);
    }

    /** @todo Not finished yet */
//    /** @test */
//    function channel_urls_redirect_with_correct_casing()
//    {
//        $this->withExceptionHandling();
//
//        $channel = create(\App\Models\Channel::class, ['name' => 'Channel']);
//
//        $route = route('channel.show', $channel);
//
//        $this->get($route)
//            ->assertStatus(200);
//
//        $route_uppercase = str_replace('Channel', 'CHANNEL', $route);
//
//        $this->get($route_uppercase)
//            ->assertStatus(200);
//
//    }

}
