<?php

namespace Tests\Feature\Auth;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccessTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function admins_can_access_telescope_pages()
    {
        $this->signInAdmin()->withExceptionHandling();

        $this->get('/telescope')
            ->assertStatus(200);
    }

    /** @test */
    function normal_user_cannot_access_telecope_pages()
    {
        $this->signIn()->withExceptionHandling();

        $this->get('/telescope')
            ->assertStatus(404);
    }

    /** @test */
    function guests_cannot_access_telescope_pages()
    {
        $this->withExceptionHandling();

        $this->get('/telescope')
            ->assertStatus(404);
    }
}
