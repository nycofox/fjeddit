<?php

namespace Tests\Feature\Auth;

use App\Events\Auth\UserHasRegistered;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RegisterUserTest extends TestCase
{

    use RefreshDatabase;

    public function setUp() : void
    {
        parent::setUp();

        Mail::fake();
    }

    /** @test */
    function users_can_register_an_account()
    {
        $response = $this->registerUser();

        $response->assertRedirect('/');

        $this->assertTrue(Auth::check());

        $this->assertCount(1, User::all());

        tap(User::first(), function ($user) {
            $this->assertEquals('JohnDoe', $user->name);
            $this->assertEquals('johndoe@example.com', $user->email);
            $this->assertTrue(Hash::check('secret', $user->password));
            $this->assertNull($user->email_verified_at);
        });
    }

    /** @test */
    function name_is_required()
    {
        $response = $this->registerUser(['name' => '']);

        $response->assertSessionHasErrors('name');

        $this->assertFalse(Auth::check());

        $this->assertCount(0, User::all());
    }

    /** @test */
    function name_is_unique()
    {
        create(User::class, ['name' => 'john']);

        $response = $this->registerUser(['name' => 'john']);

        $response->assertSessionHasErrors('name');
    }

    /** @test */
    function name_cannot_exceed_50_chars()
    {
        $response = $this->registerUser(['name' => str_repeat('a', 51)]);

        $response->assertSessionHasErrors('name');
    }

    /** @test */
    function name_is_url_safe()
    {
        $response = $this->registerUser(['name' => 'spaces and symbols!']);

        $response->assertSessionHasErrors('name');
    }

//    /** @test */
//    function name_is_case_insensitive()
//    {
//        $this->withExceptionHandling();
//
//        create(User::class, ['name' => 'JohnDoe']);
//
//        $this->from(route('register'));
//
//        $response = $this->post(route('register'), $this->validParams([
//            'name' => 'jonhdoe',
//        ]));
////        dd(User::all());
//
//        $response->assertRedirect(route('register'));
//
//        $response->assertSessionHasErrors('name');
//
//        $this->assertFalse(Auth::check());
//
//        $this->assertCount(1, User::all());
//    }

    /** @test */
    function name_cannot_be_same_as_reserved_names()
    {
        $response = $this->registerUser(['name' => 'admin']);

        $response->assertSessionHasErrors('name');
    }

    /** @test */
    function email_is_required()
    {
        $response = $this->registerUser(['email' => '']);

        $response->assertSessionHasErrors('email');
    }

    /** @test */
    function email_is_unique()
    {
        create(User::class, ['email' => 'user@host.com']);

        $response = $this->registerUser(['email' => 'user@host.com']);

        $response->assertSessionHasErrors('email');
    }

    /** @test */
    function email_cannot_exceed_255_chars()
    {
        $response = $this->registerUser([
            'email' => substr(str_repeat('a', 256) . '@example.com', -256)
        ]);

        $response->assertSessionHasErrors('email');
    }

    /** @test */
    function password_is_required()
    {
        $response = $this->registerUser(['password' => '']);

        $response->assertSessionHasErrors('password');
    }

    /** @test */
    function password_must_be_confirmed()
    {
        $response = $this->registerUser(['password' => 'foobar', 'password_confirmation' => 'barfoo']);

        $response->assertSessionHasErrors('password');
    }

    /** @test */
    function password_must_be_6_chars()
    {
        $response = $this->registerUser(['password' => 'abcde', 'password_confirmation' => 'abcde']);

        $response->assertSessionHasErrors('password');
    }

    /** @test */
    function user_registration_triggers_an_event()
    {
        Event::fake();

        $this->registerUser();

        Event::assertDispatched(Registered::class);
    }

    /**
     * Valid parameters for registering a new user
     *
     * @param array $overrides
     * @return array
     */
    private function validParams($overrides = [])
    {
        return array_merge([
            'name' => 'JohnDoe',
            'email' => 'johndoe@example.com',
            'password' => 'secret',
            'password_confirmation' => 'secret',
        ], $overrides);
    }

    /**
     * Register a new user
     *
     * @param array $overrides
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    private function registerUser($overrides = [])
    {
        $this->withExceptionHandling();

        $user = $this->validParams($overrides);

        return $this->post(route('register'), $user);
    }

}
