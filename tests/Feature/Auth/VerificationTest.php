<?php

namespace Tests\Feature\Auth;

use Illuminate\Support\Facades\URL;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class VerificationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function unverified_users_will_see_a_warning_on_the_front_page()
    {
        $user = create(\App\Models\User::class, ['email_verified_at' => null]);

        $this->signIn($user);

        $this->get('/')
            ->assertSee('You have not verified your email.');
    }

    /** @test */
    function verified_users_will_not_see_a_warning_on_the_front_page()
    {
        $this->signIn();

        $this->get('/')
            ->assertDontSee('You have not verified your email.');
    }

    /** @test */
    function guests_will_not_see_a_warning_on_the_front_page()
    {
        $this->get('/')
            ->assertDontSee('You have not verified your email.');
    }

    /** @test */
    function users_can_verify_their_email()
    {
        $user = create(\App\Models\User::class, ['email_verified_at' => null]);

        $this->signIn($user);

        $this->assertNull($user->email_verified_at);

        $this->get(URL::temporarySignedRoute(
            'verification.verify',
            now()->addMinutes(30),
            [$user->getKey()]
        ));

        $this->assertNotNull($user->fresh()->email_verified_at);
    }

}
