<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminAccessTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function a_user_with_admin_privileges_can_see_the_admin_dashboard()
    {
        $this->signInAdmin()->withExceptionHandling();

        $this->get(route('admin.dashboard'))
            ->assertStatus(200);
    }

    /** @test */
    function normal_users_cannot_see_the_admin_dashboard()
    {
        $this->signIn()->withExceptionHandling();

        $this->get(route('admin.dashboard'))
            ->assertStatus(404);
    }

    /** @test */
    function guests_cannot_see_the_admin_dashboard()
    {
        $this->withExceptionHandling();

        $this->get(route('admin.dashboard'))
            ->assertStatus(404);
    }

}
