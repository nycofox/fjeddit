<?php

namespace Tests\Feature\Admin;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function admins_can_list_users()
    {
        $users = create(\App\Models\User::class, [], 2);

        $this->signInAdmin();

        $this->get(route('admin.user.index'))
            ->assertSee($users[0]->username)
            ->assertSee($users[1]->username);
    }
}
