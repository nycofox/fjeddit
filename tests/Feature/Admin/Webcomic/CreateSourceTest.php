<?php

namespace Tests\Feature\Admin\Webcomic;

use App\Models\Webcomic;
use App\Models\WebcomicSource;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateSourceTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function a_new_source_can_be_added_to_a_webcomic()
    {
        $this->addSource();

        $this->assertCount(1, WebcomicSource::all());
    }

    /** @test */
    function a_source_requires_a_slug()
    {
        $response = $this->addSource(['slug' => '']);

        $response->assertSessionHasErrors(['slug']);
    }

    /** @test */
    function a_source_requires_a_homepage()
    {
        $response = $this->addSource(['homepage' => '']);

        $response->assertSessionHasErrors(['homepage']);
    }

    /** @test */
    function source_type_must_be_valid()
    {
        $response = $this->addSource(['type' => 'x']);

        $response->assertSessionHasErrors(['type']);
    }

    /** @test */
    function source_searchpage_must_be_valid_if_it_is_set()
    {
        $response = $this->addSource(['searchpage' => 'htxx_a:/so_notvalid']);

        $response->assertSessionHasErrors(['searchpage']);

        $this->addSource(['searchpage' => '']);

        $this->assertCount(1, WebcomicSource::all());
    }

    private function addSource($overrides = [])
    {
        $this->withExceptionHandling()->signInAdmin();

        $webcomic = create(Webcomic::class);

        $source = make(WebcomicSource::class, array_merge(['webcomic_id' => null], $overrides));

        return $this->post(route('admin.webcomics.source.store', $webcomic), $source->toArray());
    }
}
