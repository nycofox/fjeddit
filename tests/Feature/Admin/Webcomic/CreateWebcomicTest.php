<?php

namespace Tests\Feature\Admin\Webcomic;

use App\Models\Webcomic;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateWebcomicTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function only_admins_can_access_the_webcomics_dashboard()
    {
        $this->withExceptionHandling();

        $this->get(route('admin.webcomics.dashboard'))
            ->assertStatus(404);

        $this->signIn();

        $this->get(route('admin.webcomics.dashboard'))
            ->assertStatus(404);

        $this->signInAdmin();

        $this->get(route('admin.webcomics.dashboard'))
            ->assertStatus(200);
    }

    /** @test */
    function a_new_webcomic_can_be_created()
    {
        $this->addWebcomic();

        $this->assertCount(1, Webcomic::all());
    }

    /** @test */
    function a_new_webcomic_requires_a_name()
    {
        $response = $this->addWebcomic(['name' => '']);

        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    function a_new_webcomic_requires_a_slug()
    {
        $response = $this->addWebcomic(['slug' => '']);

        $response->assertSessionHasErrors(['slug']);
    }

    /** @test */
    function a_webcomic_name_must_be_unique()
    {
        $webcomic = create(Webcomic::class);

        $response = $this->addWebcomic(['name' => $webcomic->name]);

        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    function a_webcomic_slug_must_be_unique()
    {
        $webcomic = create(Webcomic::class);

        $response = $this->addWebcomic(['slug' => $webcomic->slug]);

        $response->assertSessionHasErrors(['slug']);
    }

    /**
     * Add a new Webcomic
     *
     * @param array $overrides
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    private function addWebcomic($overrides = [])
    {
        $this->withExceptionHandling()->signInAdmin();

        $webcomic = make(Webcomic::class, $overrides);

        return $this->post(route('admin.webcomics.store'), $webcomic->toArray());
    }

}
