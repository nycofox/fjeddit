<?php

namespace Tests\Feature\Admin\Webcomic;

use App\Models\Webcomic;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShowWebcomicTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_will_list_all_webcomics()
    {
        $this->signInAdmin();

        $webcomic1 = create(Webcomic::class);
        $webcomic2 = create(Webcomic::class);

        $this->get(route('admin.webcomics.index'))
            ->assertSee($webcomic1->name)
            ->assertSee($webcomic2->name);
    }

}
