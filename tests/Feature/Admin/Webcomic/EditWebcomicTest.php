<?php

namespace Tests\Feature\Admin\Webcomic;

use App\Models\Webcomic;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EditWebcomicTest extends TestCase
{
    use RefreshDatabase;

    private $webcomic;

    public function setUp(): void
    {
        parent::setUp();

        $this->webcomic = create(Webcomic::class);
    }

    /** @test */
    function non_administrators_cannot_edit_a_webcomic()
    {
        $this->withExceptionHandling();

        $this->post(route('admin.webcomics.edit', $this->webcomic))
            ->assertStatus(404);

        $this->signIn();

        $this->post(route('admin.webcomics.edit', $this->webcomic))
            ->assertStatus(404);
    }

    /** @test */
    function administrators_can_edit_the_name()
    {
        $this->signInAdmin();

        $this->post(route('admin.webcomics.edit', $this->webcomic), [
            'name' => 'Changed',
            'slug' => $this->webcomic->slug,      // These are here to make sure the validation ignores the unique
            'author' => $this->webcomic->author,  // constraint for updates
        ]);

        $this->assertEquals('Changed', $this->webcomic->fresh()->name);
    }

    /** @test */
    function administrators_can_edit_the_slug()
    {
        $this->signInAdmin();

        $this->post(route('admin.webcomics.edit', $this->webcomic), [
            'name' => $this->webcomic->name,
            'slug' => 'new-slug',
            'author' => $this->webcomic->author,
        ]);

        $this->assertEquals('new-slug', $this->webcomic->fresh()->slug);
    }

    /** @test */
    function administrators_can_edit_the_author()
    {
        $this->signInAdmin();

        $this->post(route('admin.webcomics.edit', $this->webcomic), [
            'name' => $this->webcomic->name,
            'slug' => $this->webcomic->slug,
            'author' => 'New Author',
        ]);

        $this->assertEquals('New Author', $this->webcomic->fresh()->author);
    }

    /** @test */
    function administrators_can_edit_all_variables()
    {
        $this->signInAdmin();

        $this->post(route('admin.webcomics.edit', $this->webcomic), [
            'name' => 'Changed',
            'slug' => 'new-slug',
            'author' => 'New Author'
        ]);

        $this->assertEquals('Changed', $this->webcomic->fresh()->name);
        $this->assertEquals('new-slug', $this->webcomic->fresh()->slug);
        $this->assertEquals('New Author', $this->webcomic->fresh()->author);
    }

    /** @test */
    function it_cannot_have_the_same_slug_or_name_as_another_webcomic()
    {
        $this->signInAdmin();

        $this->withExceptionHandling();

        $another = create(Webcomic::class);

        $this->post(route('admin.webcomics.edit', $this->webcomic), [
            'name' => $another->name,
            'slug' => $another->slug,
            'author' => 'New Author'
        ])->assertSessionHasErrors(['slug', 'name']);
    }
}
