<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FrontpageTest extends TestCase
{

    use RefreshDatabase;

    private $thread;

    public function setUp() : void
    {
        parent::setUp();

        $this->thread = create(\App\Models\Thread::class);
    }

    /** @test */
    function guests_should_see_the_front_page()
    {
        $this->get('/')
            ->assertStatus(200);
    }

    /** @test */
    function logged_in_users_should_see_the_front_page()
    {
        $this->signIn();

        $this->get('/')
            ->assertStatus(200);
    }

    /** @test */
    function a_public_thread_should_be_visible_on_the_front_page()
    {
        $this->get('/')
            ->assertSee($this->thread->title);
    }

    /** @test */
    function a_thread_on_the_front_page_should_link_to_the_thread_view()
    {
        $this->get('/')
            ->assertSee(route('thread.show', [$this->thread->channel, $this->thread]));
    }

    /** @test */
    function a_thread_should_include_the_username_of_its_creator()
    {
        $this->get('/')
            ->assertSee($this->thread->user->name);
    }

    /** @test */
    function a_thread_should_link_to_the_user_profile()
    {
        $this->get('/')
            ->assertSee(route('profile', $this->thread->user));
    }
}
