<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserProfileTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    function a_user_has_a_profile()
    {
        $user = create(\App\Models\User::class);

        $response = $this->get(route('profile', $user));

        $response->assertStatus(200);

        $response->assertSee($user->name);
    }
}
