<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserSettingsTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    /** @test */
    function a_user_can_see_their_settings_page()
    {
        $this->signInTestUser();

        $this->get(route('settings'))
            ->assertStatus(200)
            ->assertSee('Settings');
    }

    /** @test */
    function guests_cannot_view_the_settings_page()
    {
        $this->withExceptionHandling();

        $this->get(route('settings'))->assertRedirect(route('login'));
    }

    /** @test */
    function a_user_can_change_their_password()
    {
        $this->signInTestUser();

        $this->assertTrue(Hash::check('secret', $this->user->password));

        $this->postPassword()->assertRedirect(route('settings'));

        $this->assertTrue(Hash::check('foobar', $this->user->fresh()->password));
    }

    /** @test */
    function current_password_must_match()
    {
        $this->signInTestUser();

        $this->postPassword(['oldpassword' => 'wrongpassword'])
            ->assertSessionHasErrors('message');
    }

    /** @test */
    function new_password_must_be_confirmed()
    {
        $this->signInTestUser();

        $this->postPassword(['newpassword_confirmation' => 'wrongpassword'])
            ->assertSessionHasErrors('newpassword');
    }

    /**
     * Sign in a test user with the password "secret"
     */
    private function signInTestUser(): void
    {
        $this->user = create(User::class, [
            'password' => Hash::make('secret')
        ]);

        $this->signIn($this->user)->withExceptionHandling();
    }

    /**
     * Default password fields
     *
     * @param array $overrides
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    private function postPassword($overrides = [])
    {
        return $this->post(route('settings.storepassword'), array_merge([
            'oldpassword' => 'secret',
            'newpassword' => 'foobar',
            'newpassword_confirmation' => 'foobar'
        ], $overrides));
    }
}
