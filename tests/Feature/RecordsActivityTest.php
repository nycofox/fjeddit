<?php

namespace Tests\Feature;

use App\Models\Thread;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RecordsActivityTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function creating_a_thread_records_an_activity()
    {
        $thread = create(Thread::class);

        $this->assertCount(1, $thread->activity);

        tap($thread->activity->last(), function ($activity) {
            $this->assertEquals('created_thread', $activity->description);
            $this->assertNull($activity->changes);
        });
    }

    /** @test */
    function changing_a_thread_records_the_changes()
    {
        $thread = create(Thread::class);

        $originalTitle = $thread->title;

        $thread->update(['title' => 'Changed']);

        tap($thread->activity->last(), function ($activity) use ($originalTitle) {
            $this->assertEquals('updated_thread', $activity->description);
            $expected = [
                'before' => ['title' => $originalTitle],
                'after' => ['title' => 'Changed']
            ];
            $this->assertEquals($expected, $activity->changes);
        });
    }

    /** @test */
    function soft_deleting_a_thread_records_an_activity()
    {
        $thread = create(Thread::class);

        $thread->delete();

        tap($thread->fresh()->activity->last(), function ($activity) {
            $this->assertEquals('deleted_thread', $activity->description);
        });
    }
}
