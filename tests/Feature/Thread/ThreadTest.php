<?php

namespace Tests\Feature\Thread;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ThreadTest extends TestCase
{
    use RefreshDatabase;

    private $thread;

    public function setUp(): void
    {
        parent::setUp();

        $this->thread = create(\App\Models\Thread::class);
    }

    /** @test */
    function a_thread_has_a_view()
    {
        $this->get(route('thread.show', [$this->thread->channel, $this->thread]))
            ->assertStatus(200)
            ->assertSee($this->thread->title)
            ->assertSee($this->thread->body);
    }

    /** @test */
    function a_thread_links_to_the_creator()
    {
        $this->get(route('thread.show', [$this->thread->channel, $this->thread]))
            ->assertSee(route('profile', $this->thread->user));
    }

    /** @test */
    function a_thread_will_display_replies()
    {
        $reply = create(\App\Models\Reply::class, ['thread_id' => $this->thread->id]);

        $this->get(route('thread.show', [$this->thread->channel, $this->thread]))
            ->assertSee($reply->body);
    }

    /** @test */
    function a_thread_will_count_the_number_of_views_it_has()
    {
        $this->assertEquals(0, $this->thread->views);

        $this->get(route('thread.show', [$this->thread->channel, $this->thread]));

        $this->assertEquals(1, $this->thread->fresh()->views);
    }

    /** @test */
    function a_thread_will_show_the_number_of_replies_it_has()
    {
        create(\App\Models\Reply::class, ['thread_id' => $this->thread->id], 3);

        $this->get('/')
            ->assertSee('3 comments');
    }

}
