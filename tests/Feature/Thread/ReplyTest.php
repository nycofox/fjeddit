<?php

namespace Tests\Feature\Thread;

use App\Models\Reply;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReplyTest extends TestCase
{
    use RefreshDatabase;

    private $thread;

    public function setUp() : void
    {
        parent::setUp();

        $this->thread = create(\App\Models\Thread::class);
    }

    /** @test */
    function a_thread_will_show_replies()
    {
        $reply = create(Reply::class, ['thread_id' => $this->thread->id]);

        $this->get(route('thread.show', [$this->thread->channel, $this->thread]))
            ->assertSee($reply->body);
    }

    /** @test */
    function a_guest_cannot_reply_to_a_thread()
    {
        $this->withExceptionHandling();
        $this->post(route('thread.reply', [$this->thread->channel, $this->thread]))
            ->assertStatus(302)
            ->assertRedirect(route('login'));
    }

    /** @test */
    function a_user_can_reply_to_a_thread()
    {
        $this->withExceptionHandling()->signIn();

        $reply = make(Reply::class, ['thread_id' => null])->toArray();

        $this->post(route('thread.reply', [$this->thread->channel, $this->thread]), $reply)
            ->assertRedirect(route('thread.show', [$this->thread->channel, $this->thread]));

        $this->assertCount(1, Reply::all());
    }

    /** @test */
    function a_reply_body_is_required()
    {
        $this->withExceptionHandling()->signIn();

        $reply = make(Reply::class, ['body' => null, 'thread_id' => null])->toArray();

        $this->post(route('thread.reply', [$this->thread->channel, $this->thread]), $reply)
            ->assertSessionHasErrors('body');
    }
}
