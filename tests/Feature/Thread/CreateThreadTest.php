<?php

namespace Tests\Feature\Thread;

use App\Models\Channel;
use App\Models\Thread;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateThreadTest extends TestCase
{
    use RefreshDatabase;

    private $channel;

    public function setUp() : void
    {
        parent::setUp();

        $this->channel = create(Channel::class);
    }

    /** @test */
    public function guests_may_not_create_threads()
    {
        $this->withExceptionHandling();
        $this->post(route('thread.store', $this->channel))
            ->assertStatus(302)
            ->assertRedirect(route('login'));
    }

    /** @test */
    function a_thread_can_be_created()
    {
        $this->publishThread();

        $this->assertCount(1, Thread::all());
    }

    /** @test */
    function a_thread_requires_a_valid_title()
    {
        $response = $this->publishThread(['title' => '']);

        $response->assertSessionHasErrors('title');
    }

    /** @test */
    function a_thread_cannot_be_published_to_a_nonexistant_channel()
    {
        $response = $this->publishThread(['channel_id' => 999]);

        $response->assertSessionHasErrors('channel_id');
    }

    protected function publishThread($overrides = [])
    {
        $this->withExceptionHandling()->signIn();
        $thread = make(Thread::class, $overrides);
        return $this->post(route('thread.store', $this->channel), $thread->toArray());
    }
}
