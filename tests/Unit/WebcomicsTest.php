<?php

namespace Tests\Unit\Entertainment;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WebcomicsTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    function a_webcomic_has_sources()
    {
        $webcomic = create(\App\Models\Webcomic::class);
        $this->assertInstanceOf(
            'Illuminate\Database\Eloquent\Collection',
            $webcomic->sources
        );
    }
}
