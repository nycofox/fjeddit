<?php

namespace Tests\Unit;

use App\Models\ImageLink;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ImageLinkTest extends TestCase
{
    use RefreshDatabase;

    protected $imagelink;

    public function setUp() : void
    {
        parent::setUp();
        $this->imagelink = create(\App\Models\ImageLink::class);
    }

    /** @test */
    function an_image_link_has_an_image()
    {
        $this->assertInstanceOf(\App\Models\Image::class, $this->imagelink->image);
    }

    /** @test */
    function an_image_link_has_an_owner()
    {
        $this->assertInstanceOf(\App\Models\User::class, $this->imagelink->user);
    }

    /** @test */
    function an_image_link_can_be_soft_deleted()
    {
        $this->assertCount(1, ImageLink::all());

        $this->imagelink->delete();

        $this->assertCount(0, ImageLink::all());

        $this->assertCount(1, ImageLink::withTrashed()->get());

        $this->assertNotNull($this->imagelink->fresh()->deleted_at);
    }
}
