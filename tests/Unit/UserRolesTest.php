<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserRolesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function a_user_can_be_assign_a_role()
    {
        $user = create(User::class);

        $user->assignRole('admin');

        $this->assertTrue($user->hasRole('admin'));
    }

    /** @test */
    function a_user_can_be_unassigned_an_existing_role()
    {
        $user = create(User::class);

        $user->assignRole('admin');

        $this->assertTrue($user->hasRole('admin'));

        $user->removeRole('admin');

        $this->assertFalse($user->fresh()->hasRole('admin'));
    }

    /** @test */
    function a_user_will_not_say_it_has_a_role_it_doesnt_have()
    {
        $user = create(User::class);

        $this->assertFalse($user->hasRole('trololo'));
    }
}
