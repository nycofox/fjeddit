<?php

namespace Tests\Feature\User;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class KarmaTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function a_user_can_receive_karma()
    {
        $user = create(\App\Models\User::class);

        $this->assertEquals(0, $user->karma);

        $user->giveKarma();

        $this->assertEquals(1, $user->fresh()->karma);

        $user->giveKarma(3);

        $this->assertEquals(4, $user->fresh()->karma);
    }

    /** @test */
    function a_user_can_lose_karma()
    {
        $user = create(\App\Models\User::class, ['karma' => 5]);

        $this->assertEquals(5, $user->karma);

        $user->removeKarma();

        $this->assertEquals(4, $user->fresh()->karma);

        $user->removeKarma(6);

        $this->assertEquals(-2, $user->fresh()->karma);
    }
}
