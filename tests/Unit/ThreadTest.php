<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ThreadTest extends TestCase
{
    use RefreshDatabase;

    protected $thread;

    public function setUp() : void
    {
        parent::setUp();
        $this->thread = create(\App\Models\Thread::class);
    }

    /** @test */
    function a_thread_has_a_creator()
    {
        $this->assertInstanceOf(\App\Models\User::class, $this->thread->user);
    }

    /** @test */
    function a_thread_has_replies()
    {
        $this->assertInstanceOf(
            'Illuminate\Database\Eloquent\Collection',
            $this->thread->replies
        );
    }

    /** @test */
    function a_thread_belongs_to_a_channel()
    {
        $this->assertInstanceOf(\App\Models\Channel::class, $this->thread->channel);
    }

    /** @test */
    function a_thread_contains_replies()
    {
        $reply1 = create(\App\Models\Reply::class, ['thread_id' => $this->thread->id]);
        $reply2 = create(\App\Models\Reply::class, ['thread_id' => $this->thread->id]);

        $this->assertTrue($this->thread->replies->contains($reply1));
        $this->assertTrue($this->thread->replies->contains($reply2));
    }

    /** @test */
    function a_thread_can_be_soft_deleted()
    {
        $this->assertNull($this->thread->deleted_at);

        $this->thread->delete();

        $this->assertNotNull($this->thread->fresh()->deleted_at);
    }

}
