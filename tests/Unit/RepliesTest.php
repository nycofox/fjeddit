<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RepliesTest extends TestCase
{
    use RefreshDatabase;

    protected $reply;

    public function setUp() : void
    {
        parent::setUp();
        $this->reply = create(\App\Models\Reply::class);
    }

    /** @test */
    function a_reply_belongs_to_a_thread()
    {
        $this->assertInstanceOf(\App\Models\Thread::class, $this->reply->thread);
    }

    /** @test */
    function a_reply_has_an_owner()
    {
        $this->assertInstanceOf(\App\Models\User::class, $this->reply->user);
    }
}
