<?php

namespace Tests\Feature\User;

use App\Models\Album;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AlbumTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function new_users_will_have_profile_pictures_album_created()
    {
        $user = create(User::class);

        $this->assertCount(1, Album::all());

        $this->assertCount(1, $user->albums);
    }
}
