<?php

namespace Tests\Unit;

use App\Models\Channel;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ChannelTest extends TestCase
{

    use RefreshDatabase;

    protected $channel;

    public function setUp() : void
    {
        parent::setUp();
        $this->channel = create(Channel::class);
    }

    /** @test */
    function a_channel_consists_of_threads()
    {
        $thread = create(\App\Models\Thread::class, ['channel_id' => $this->channel->id]);

        $this->assertTrue($this->channel->threads->contains($thread));
    }

    /** @test */
    function a_channel_has_a_creator()
    {
        $this->assertInstanceOf(\App\Models\User::class, $this->channel->creator);
    }

    /** @test */
    function a_channel_has_an_owner()
    {
        $this->assertInstanceOf(\App\Models\User::class, $this->channel->owner);
    }
}
