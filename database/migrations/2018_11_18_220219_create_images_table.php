<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('filename');
            $table->string('filename_thumbnail'); // Link to filename for generated thumbnail
            $table->string('filename_sized'); // Link to filename for sized version. If the original is small, same as filename
            $table->text('ocr')->nullable();
            $table->unsignedBigInteger('filesize');
            $table->string('md5', 32); // md5 hash of image
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
