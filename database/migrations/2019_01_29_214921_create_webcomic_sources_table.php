<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebcomicSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webcomic_sources', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('webcomic_id');
            $table->string('slug')->unique();
            $table->string('homepage');
            $table->string('type', 1); // g=generate, s=search
            $table->string('url'); // Either url for generate, or string for search
            $table->string('searchpage')->nullable(); // Page to search in
            $table->timestamp('last_scrape_attempt_at')->nullable();
            $table->timestamp('last_scraped_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webcomic_sources');
    }
}
