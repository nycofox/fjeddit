<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 30)->create();

        if (env('ADMIN_NAME') != '') {
            $admin = User::create([
                'name' => env('ADMIN_NAME', 'Admin'),
                'email' => env('ADMIN_EMAIL', 'admin@example.com'),
                'password' => \Hash::make(env('ADMIN_PASSWORD', 'secret')),
                'email_verified_at' => now(),
            ]);

            $admin->assignRole('admin');
        }
    }
}
