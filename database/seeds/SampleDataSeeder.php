<?php

use App\Models\Channel;
use App\Models\Reply;
use App\Models\Thread;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class SampleDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        $this->channels()->content();

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Seed the channels table.
     */
    protected function channels()
    {
        Channel::truncate();

        collect([
            [
                'name' => 'Fun',
                'description' => 'Just general fun.'
            ],
            [
                'name' => 'Cats',
                'description' => 'Anything cats!'
            ],
        ])->each(function ($channel) {
            factory(Channel::class)->create([
                'name' => $channel['name'],
                'description' => $channel['description'],
            ]);
        });
        return $this;
    }

    /**
     * Seed the thread-related tables.
     */
    protected function content()
    {
        Thread::truncate();

        Reply::truncate();

        factory(Thread::class, 50)->states('from_existing_channels_and_users')->create();

        factory(Reply::class, 200)->states('from_existing_threads_and_users')->create();
    }

}
