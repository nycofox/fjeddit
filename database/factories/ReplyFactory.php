<?php

use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(App\Models\Reply::class, function (Faker $faker) {
    return [
        'thread_id' => function () {
            return factory(\App\Models\Thread::class)->create()->id;
        },
        'user_id' => function () {
            return factory(\App\Models\User::class)->create()->id;
        },
        'body' => $faker->paragraph,
    ];
});

$factory->state(App\Models\Reply::class, 'from_existing_threads_and_users', function ($faker) {
    $title = $faker->sentence;
    return [
        'user_id' => function () {
            return \App\Models\User::all()->random()->id;
        },
        'thread_id' => function () {
            return \App\Models\Thread::all()->random()->id;
        },
        'body' => $title,
    ];
});
