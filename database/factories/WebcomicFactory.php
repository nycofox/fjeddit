<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Webcomic::class, function (Faker $faker) {
    return [
        'slug' => strtolower($faker->word),
        'name' => $faker->word,
        'author' => $faker->name,
    ];
});
