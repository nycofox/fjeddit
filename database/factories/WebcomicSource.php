<?php

use Faker\Generator as Faker;

$factory->define(App\Models\WebcomicSource::class, function (Faker $faker) {
    return [
        'slug' => $faker->slug,
        'homepage' => $faker->url,
        'type' => 'g',
        'url' => $faker->url,
        'searchpage' => $faker->url,
        'webcomic_id' => function () {
            return factory(\App\Models\Webcomic::class)->create()->id;
        },
    ];
});
