<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Thread::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(\App\Models\User::class)->create()->id;
        },
        'channel_id' => function () {
            return factory(\App\Models\Channel::class)->create()->id;
        },
        'title' => $faker->sentence,
        'body' => $faker->paragraph,
    ];
});

$factory->state(App\Models\Thread::class, 'from_existing_channels_and_users', function ($faker) {
    $title = $faker->sentence;
    return [
        'user_id' => function () {
            return \App\Models\User::all()->random()->id;
        },
        'channel_id' => function () {
            return \App\Models\Channel::all()->random()->id;
        },
        'title' => $title,
    ];
});
