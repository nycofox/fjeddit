<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Channel::class, function (Faker $faker) {

    $user = factory(\App\Models\User::class)->create();

    return [
        'creator_id' => $user->id,
        'owner_id' => $user->id,
        'name' => $faker->firstName,
        'description' => $faker->sentence,

    ];
});
