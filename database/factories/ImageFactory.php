<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Image::class, function (Faker $faker) {
    return [
        'filename' => $faker->word . $faker->fileExtension,
        'filename_thumbnail' => $faker->word . $faker->fileExtension,
        'filename_sized' => $faker->word . $faker->fileExtension,
        'filesize' => random_int(1, 100000),
        'md5' => md5(str_random(10)),
    ];
});
