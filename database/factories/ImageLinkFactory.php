<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ImageLink::class, function (Faker $faker) {
    return [
        'image_id' => function () {
            return factory(\App\Models\Image::class)->create()->id;
        },
        'user_id' => function () {
            return factory(\App\Models\User::class)->create()->id;
        },
        'title' => $faker->word,
        'description' => $faker->sentence,
    ];
});
